﻿using ConsoleAdministrador.Controladores;
using ConsoleAdministrador.Exibição;
using Modelos;
using Modelos.ModelosExibição;
using Modelos.Repositórios;
using System;

namespace ConsoleAdministrador
{
    class Program
    {
        static void Main(string[] args)
        {
            while(true)
            {
                var opção = MenusExibição.MenuPrincipal();

                if(opção == 0)
                {
                    MensagensExibição.PedirInteração();
                    return;
                }
                else if(opção == 1)
                {
                    var dadosLogin = FormuláriosExibição.FormulárioLogin();
                    UsuárioModeloExibição dadosUsuário;

                    MensagensExibição.Esperar();

                    using(var contexto = new AplicaçãoContext())
                    {
                        var repositório = new UsuáriosRepositório(contexto);
                        dadosUsuário = repositório.Logar(dadosLogin);
                    }

                    if(!(dadosUsuário == null))
                    {
                        PainelControlador.PainelAdministrativo(dadosUsuário);
                    }
                    else
                    {
                        MensagensExibição.UsuárioNãoEncontrado();
                    }

                    MensagensExibição.PedirInteração();
                }
            }
        }
    }
}
