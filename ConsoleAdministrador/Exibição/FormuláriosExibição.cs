﻿using Modelos.Modelos;
using Modelos.ModelosExibição;
using SenhaConsole;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace ConsoleAdministrador.Exibição
{
    public static class FormuláriosExibição
    {
        private static string LerCampo(string campo, bool secreto = false)
        {
            string retorno;
            Console.Write($"{campo}: ");
            if (secreto)
            {
                retorno = MascararSenhaConsole.LerSenha();
            }
            else
            {
                retorno = Console.ReadLine();
            }
            return retorno;
        }
        public static LoginModeloExibição FormulárioLogin()
        {
            var usuário = LerCampo("Usuário");
            var senha = LerCampo("Senha", true);

            return new LoginModeloExibição() { NomeUsuário = usuário, Senha = senha };
        }



        public static UsuárioModelo FormulárioCriarUsuário()
        {
            var nome = LerCampo("Nome");
            var usuário = LerCampo("Usuário");
            var senha = LerCampo("Senha", true);

            return new UsuárioModelo()
            {
                Nome = nome,
                Usuário = usuário,
                Senha = senha,
            };
        }

        public static CélulaModelo FormulárioCriarCélula()
        {
            var mac = LerCampo("Endereço MAC");
            var latitude = LerCampo("Latitude");
            var longitude = LerCampo("Longitude");
            var endereço = LerCampo("Endereço");

            return new CélulaModelo()
            {
                EndereçoMac = mac,
                Latitude = ParaDouble(latitude),
                Longitude = ParaDouble(longitude),
                Endereço = endereço
            };
        }

        private static double ParaDouble(string dado)
        {
            try
            {
                return Double.Parse(dado.Replace(',', '.'), CultureInfo.InvariantCulture);
            }
            catch(Exception)
            {
                return 0;
            }
        }

        public static int FormulárioId()
        {
            try
            {
                return Int32.Parse(LerCampo("Id"));
            }
            catch (Exception)
            {
                return -1;
            }
        }
    }
}
