﻿using Modelos.Modelos;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleAdministrador.Exibição
{
    public static class ListagemExibição
    {
        public static void ListarRequisições(List<RequisiçãoModelo> lista)
        {
            Console.Clear();

            for (int i = 0; i < lista.Count; i++)
            {
                var item = lista[i];
                Console.WriteLine($"{i + 1} | Id: {item.Id} | Momento: {item.Momento}");
            }
        }

        public static void ListarUsuários(List<UsuárioModelo> lista)
        {
            Console.Clear();

            for (int i = 0; i < lista.Count; i++)
            {
                var item = lista[i];
                Console.WriteLine($"{i + 1} | Id: {item.Id} | Nome: {item.Nome} | Usuário : {item.Usuário}");
            }
        }

        public static void ListarCélulas(List<CélulaModelo> lista)
        {
            Console.Clear();

            for (int i = 0; i < lista.Count; i++)
            {
                var item = lista[i];
                Console.WriteLine($"{i + 1} | Id: {item.Id} | MAC : {item.EndereçoMac} | Endereço: {item.Endereço} | " +
                    $"Latitude: {item.Latitude} | Longitude: {item.Longitude}");
            }
        }
    }
}
