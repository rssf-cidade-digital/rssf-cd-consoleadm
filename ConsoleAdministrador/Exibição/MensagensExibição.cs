﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleAdministrador.Exibição
{
    public static class MensagensExibição
    {
        public static void UsuárioNãoEncontrado()
        {
            Console.WriteLine("Usuário não encontrado. Tente novamente");
        }

        public static void Esperar()
        {
            Console.WriteLine("\nAguarde...");
        }

        public static void EntradaInválida()
        {
            Console.WriteLine("Entrada inválida! Assumindo resposta padrão 0.");
        }

        public static void EntradaInválidaSemPadrão()
        {
            Console.WriteLine("Entrada inválida!");
        }

        public static void PedirInteração()
        {
            Console.WriteLine("Aperte ENTER para seguir...");
            Console.ReadLine();
        }

        public static void Sucesso()
        {
            Console.WriteLine("Operação realizada com sucesso.");
        }

        public static void DeixarEmBrancoAtualizar()
        {
            Console.WriteLine("Deixe em branco para não modificar o valor do campo correspondente. Se for numérico, deixe 0 para " +
                "não modificar.");
        }

        public static void CélulaNãoEncontrada()
        {
            Console.WriteLine("Célula Não Encontrada!");
        }
    }
}
