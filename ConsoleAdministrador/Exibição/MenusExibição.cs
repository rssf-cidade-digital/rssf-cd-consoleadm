﻿using System;
using System.Collections.Generic;
using System.Text;
using Modelos.Modelos;
using Modelos.ModelosExibição;
using SenhaConsole;

namespace ConsoleAdministrador.Exibição
{
    public static class MenusExibição
    {
        private static int MontarMenu(string título, string[] opções, bool limparTela = true, bool sair = true)
        {
            if (limparTela)
            {
                Console.Clear();
            }
            Console.WriteLine($"\t\t{título}\n\n");
            for (int i = 0; i < opções.Length; i++)
            {
                Console.WriteLine($"{i + 1} - {opções[i]}");
            }
            if (sair) Console.WriteLine("0 - Sair");

            return RetornarOpção();
        }
        public static int MenuPrincipal()
        {
            return MontarMenu("Sistema de administração da RSFF Cidade Digital", new string[] { "Entrar" });
        }

        private static int RetornarOpção()
        {
            try
            {
                return Int32.Parse(Console.ReadKey(true).KeyChar.ToString());
            }
            catch (Exception)
            {
                MensagensExibição.EntradaInválida();
                return 0;
            }
        }

        public static int MenuAdministrativo(UsuárioModeloExibição usuário)
        {
            string título = $"Painel Administrativo - {usuário.Nome}";
            string[] opções =
            {
                "Usuário Administradores",
                "Usuário Requerentes",
                "Usuário Monitores",
                "Requisições",
                "Células"
            };

            return MontarMenu(título, opções);
        }
        public static int MenuCRUD(bool criar, bool ler, bool atualizar, bool deletar)
        {
            List<string> opções = new List<string>();

            if (criar)
            {
                opções.Add("Criar");
            }

            if (ler)
            {
                opções.Add("Listar");
            }

            if (atualizar)
            {
                opções.Add("Atualizar");
            }

            if (deletar)
            {
                opções.Add("Deletar");
            }

            return MontarMenu("Opções de CRUD disponíveis", opções.ToArray(), false);
        }
    }
}
