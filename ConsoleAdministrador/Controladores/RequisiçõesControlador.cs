﻿using ConsoleAdministrador.Exibição;
using Modelos;
using Modelos.Modelos;
using Modelos.Repositórios;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleAdministrador.Controladores
{
    public static class RequisiçõesControlador
    {
        public static void PainelRequisições()
        {
            bool sair = false;
            do
            {
                var opção = MenusExibição.MenuCRUD(true, true, false, false);

                if (opção == 1)
                {
                    CriarRequisição();
                }
                else if (opção == 2)
                {
                    ListarRequisições();
                }
                else
                {
                    sair = true;
                }
                MensagensExibição.PedirInteração();
            } while (!sair);
        }

        private static void CriarRequisição()
        {
            var requisição = new RequisiçãoModelo() { Momento = DateTime.Now };
            using (var contexto = new AplicaçãoContext())
            {
                var repositório = new RequisiçõesRepositório(contexto);
                repositório.InserirUm(requisição);
            }

            MensagensExibição.Sucesso();
        }

        private static void ListarRequisições()
        {
            List<RequisiçãoModelo> lista;
            using (var contexto = new AplicaçãoContext())
            {
                var repositório = new RequisiçõesRepositório(contexto);
                lista = repositório.RetornarLista();
            }

            ListagemExibição.ListarRequisições(lista);
        }
    }
}
