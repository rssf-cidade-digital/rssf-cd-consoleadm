﻿using ConsoleAdministrador.Exibição;
using Modelos;
using Modelos.Modelos;
using Modelos.ModelosExibição;
using Modelos.Repositórios;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleAdministrador.Controladores
{
    public static class PainelControlador
    {

        public static void PainelAdministrativo(UsuárioModeloExibição usuário)
        {
            bool sair = false;
            do
            {
                var opção = MenusExibição.MenuAdministrativo(usuário);
                if (opção == 1)
                {
                    var controlador = new UsuáriosControlador(0);
                    controlador.PainelUsuários();
                }
                else if (opção == 2)
                {
                    var controlador = new UsuáriosControlador(1);
                    controlador.PainelUsuários();
                }
                else if (opção == 3)
                {
                    var controlador = new UsuáriosControlador(2);
                    controlador.PainelUsuários();
                }
                else if (opção == 4)
                {
                    RequisiçõesControlador.PainelRequisições();
                }
                else if(opção == 5)
                {
                    CélulasControlador.PainelCélulas();
                }
                else
                {
                    sair = true;
                }
            } while (!sair);
        }
    }
}
