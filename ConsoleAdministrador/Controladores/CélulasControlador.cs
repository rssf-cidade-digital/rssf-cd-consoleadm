﻿using Modelos;
using Modelos.Modelos;
using Modelos.Repositórios;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleAdministrador.Exibição
{
    public static class CélulasControlador
    {
        public static void PainelCélulas()
        {
            bool sair = false;
            do
            {
                var opção = MenusExibição.MenuCRUD(true, true, true, true);

                switch(opção)
                {
                    case 1:
                        Criar();
                        break;

                    case 2:
                        Listar();
                        break;

                    case 3:
                        Atualizar();
                        break;

                    case 4:
                        Deletar();
                        break;

                    default:
                        sair = true;
                        break;
                }

                MensagensExibição.PedirInteração();
            } while (!sair);
        }

        private static void Deletar()
        {
            var id = FormuláriosExibição.FormulárioId();

            if (id <= 0)
            {
                MensagensExibição.EntradaInválidaSemPadrão();
                return;
            }

            using(var contexto = new AplicaçãoContext())
            {
                var repositório = new CélulasRepositório(contexto);
                repositório.Remover(id);
            }

            MensagensExibição.Sucesso();
        }

        private static void Atualizar()
        {
            var id = FormuláriosExibição.FormulárioId();

            if (id <= 0)
            {
                MensagensExibição.EntradaInválidaSemPadrão();
                return;
            }

            CélulaModelo célula;

            using(var contexto = new AplicaçãoContext())
            {
                var repositório = new CélulasRepositório(contexto);
                célula = repositório.RetornarPeloId(id);
            }

            if(célula == null)
            {
                MensagensExibição.CélulaNãoEncontrada();
                return;
            }

            célula = FormuláriosExibição.FormulárioCriarCélula();

            using(var contexto = new AplicaçãoContext())
            {
                var repositório = new CélulasRepositório(contexto);
                repositório.Modificar(id, célula);
            }

            MensagensExibição.Sucesso();
        }

        private static void Listar()
        {
            List<CélulaModelo> lista;

            using(var contexto = new AplicaçãoContext())
            {
                var repositório = new CélulasRepositório(contexto);
                lista = repositório.RetornarLista();
            }

            ListagemExibição.ListarCélulas(lista);
        }

        private static void Criar()
        {
            var célula = FormuláriosExibição.FormulárioCriarCélula();

            using(var contexto = new AplicaçãoContext())
            {
                var repositório = new CélulasRepositório(contexto);
                repositório.InserirUm(célula);
            }

            MensagensExibição.Sucesso();
        }
    }
}
