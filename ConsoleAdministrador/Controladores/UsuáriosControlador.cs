﻿using ConsoleAdministrador.Exibição;
using Modelos;
using Modelos.Modelos;
using Modelos.Repositórios;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleAdministrador.Controladores
{
    public class UsuáriosControlador
    {
        private readonly int _nível;

        public UsuáriosControlador(int nível)
        {
            _nível = nível;
        }

        public void PainelUsuários()
        {
            bool sair = false;
            do
            {
                var opção = MenusExibição.MenuCRUD(true, true, true, true);

                switch (opção)
                {
                    case 1:
                        Criar();
                        break;

                    case 2:
                        Listar();
                        break;

                    case 3:
                        Atualizar();
                        break;

                    case 4:
                        Deletar();
                        break;

                    default:
                        sair = true;
                        break;
                }
                MensagensExibição.PedirInteração();
            } while (!sair);
        }

        private void Deletar()
        {
            var id = FormuláriosExibição.FormulárioId();

            if (id <= 0)
            {
                MensagensExibição.EntradaInválidaSemPadrão();
                return;
            }

            using(var contexto = new AplicaçãoContext())
            {
                var repositório = new UsuáriosRepositório(contexto);
                repositório.Remover(id);
            }

            MensagensExibição.Sucesso();
        }

        private void Atualizar()
        {
            var id = FormuláriosExibição.FormulárioId();

            if (id <= 0)
            {
                MensagensExibição.EntradaInválidaSemPadrão();
                return;
            }

            UsuárioModelo usuário;
            using (var contexto = new AplicaçãoContext())
            {
                var repositório = new UsuáriosRepositório(contexto);
                usuário = repositório.RetornarPeloId(id);
            }

            if (usuário == null)
            {
                MensagensExibição.UsuárioNãoEncontrado();
                return;
            }

            MensagensExibição.DeixarEmBrancoAtualizar();
            usuário = FormuláriosExibição.FormulárioCriarUsuário();

            using(var contexto = new AplicaçãoContext())
            {
                var repositórioUsuários = new UsuáriosRepositório(contexto);
                InserirNívelUsuário(usuário, contexto);
                repositórioUsuários.Modificar(id, usuário);
            }

            MensagensExibição.Sucesso();

        }

        private void Listar()
        {
            List<UsuárioModelo> monitores;
            using (var contexto = new AplicaçãoContext())
            {
                var repositório = new UsuáriosRepositório(contexto);
                NívelUsuárioModelo nível = RetornarNível(contexto);
                monitores = repositório.RetornarLista(u => u.Nível == nível);
            }

            ListagemExibição.ListarUsuários(monitores);
        }

        private void Criar()
        {
            var dadosUsuário = FormuláriosExibição.FormulárioCriarUsuário();
            using (var contexto = new AplicaçãoContext())
            {
                var repositório = new UsuáriosRepositório(contexto);
                InserirNívelUsuário(dadosUsuário, contexto);
                repositório.InserirUm(dadosUsuário);
            }

            MensagensExibição.Sucesso();
        }

        private void InserirNívelUsuário(UsuárioModelo usuário, AplicaçãoContext contexto)
        {
            var nível = RetornarNível(contexto);
            usuário.Nível = nível;
        }

        private NívelUsuárioModelo RetornarNível(AplicaçãoContext contexto)
        {
            var repositórioNíves = new NíveisUsuáriosRepositório(contexto);
            var nível = repositórioNíves.RetornarÚnico(n => n.Valor == _nível);
            return nível;
        }
    }
}
